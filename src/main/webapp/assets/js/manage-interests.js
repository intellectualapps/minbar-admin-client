const authToken = getCookie('pentor_authToken');
const APIBASEURL = 'https://minbarapp-admin.appspot.com/api/v1';

if (document.cookie.indexOf("pentor_authToken") === -1) {
    window.location = "/";
}
$(document).ready( function () {

    setActiveUserEmail();

    fetchCategory();

});
var imageLink = {};
var categoriesArray = [];

/**
* this sets the email of active user on the dashboard
*/
function setActiveUserEmail() {
    var activeUserEmail = getCookie('pentor_name');
    $('#activeUser').append(activeUserEmail);

}

/**
* This function fetches category to populate the category select option
*/
function fetchCategory() {
    var loader = $('#loader');
    loader.removeClass('hide');
    $.ajax({
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + authToken);
        },
        url: APIBASEURL +'/interest',
        method: 'GET'
    })
    .done(function (response) {
        var responseObj = JSON.parse(response);
        if (responseObj.status) {
            loader.addClass('hide');

        } else {
            loader.addClass('hide');
            categoriesArray = responseObj.interests ||[];
            LoadData();
        }
    })
    .fail(function (response) {
        loader.addClass('hide');
        $('.main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>Failed to load category. Please refresh your browser!</div>');
        setTimeout(function() { $(".alert").remove(); }, 5000);

    });
}

/**
*
* @constructor
* This function populates the select option for categories
*/
function LoadData() {
    var $select = $('#intCategory');
    $select.find('option:not(:first)').remove();
    $.each(categoriesArray, function (key, value) {
        $select.append('<option value=' + value.id + '>' + value.name + '</option>');
        imageLink[value.id] = value.imageLink;
    });
}


/**
* This function fetches subInterest to be used on the subinterest table
* this is done automatically when a category is selected.
*/
function fetchSubInterest() {
    var category = $("#intCategory").val();

    if(imageLink[category] != undefined)
    $('#catAvatar').attr('src', imageLink[category]);
    $('#avatarUrl').val(imageLink[category] );

    if(category === "")
    return;
    $('body').addClass("blur");
    $('body').prepend('<div id="loader"></div>');


    $.ajax({
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + authToken);
        },
        url: APIBASEURL + '/interest/sub?id=' + category,
        method: 'GET'
    })
    .done(function (response) {
        var responseObj = JSON.parse(response);
        if (responseObj.status) {
            $('body').removeClass("blur");
            $("#loader").remove();
        } else {
            updateSubinterestTable(responseObj.subInterests);
        }
        $('body').removeClass("blur");
        $("#loader").remove();
    })
    .fail(function (response) {
        $('body').removeClass("blur");
        $("#loader").remove();
        var responseObj = JSON.parse(response);
        $('.main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>Failed to load sub-category. Please Refresh your browser!</div>');
        setTimeout(function() { $(".alert").remove(); }, 5000);
    });
}

/**
* This function is called by the fetch subinterest on successful retrival of date
* to be used for populating the subinterest table
* @param subInterestList
*/
function updateSubinterestTable(subInterestList) {
    $('#result_list').html('');
    var count = 1;
    var table = document.getElementById("result_list");

    var tHead = document.createElement("thead");
    var headerTr = document.createElement("tr");
    var serialNoHeaderTh = document.createElement("th");
    serialNoHeaderTh.innerHTML = "S/No.";
    var subInterestHeaderTh = document.createElement("th");
    subInterestHeaderTh.innerHTML = "Name";

    headerTr.appendChild(serialNoHeaderTh);
    headerTr.appendChild(subInterestHeaderTh);

    tHead.appendChild(headerTr);
    table.appendChild(tHead);

    var tbody = document.createElement("tbody");
    tbody.id = "result-list-tbody";
    var connectionsLength= subInterestList.length;
    if (connectionsLength > 0) {

        for(var i = 0; i < connectionsLength; i++){
            var subInterest = subInterestList[i].name;

            var tr = document.createElement("tr");

            var countTd = document.createElement("td");
            countTd.innerHTML = count++;

            var subInterestHeaderTd = document.createElement("td");
            subInterestHeaderTd.innerHTML = "<span class='text-primary'>"+subInterest+"</span>";

            tr.appendChild(countTd);
            tr.appendChild(subInterestHeaderTd);

            tbody.appendChild(tr);
        }

    } else {
        var tr = document.createElement("tr");
        var td = document.createElement("td");
        td.innerHTML = "<h1>No subinterest found</h1>";
        td.className = "center";
        td.setAttribute("colspan", "2");
        td.setAttribute("align", "center");
        tr.appendChild(td);
        tbody.appendChild(tr);

    }
    table.appendChild(tbody);
}

/**
* This function performs a put method which update the avatar of the interest selected.
*/
function updateImage() {
    var category = $("#intCategory").val();
    var imageUrl = $('#avatarUrl').val();

    if(category == ""){
        $('.main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>Please Select the Category whose image you will want to update!</div>');
        setTimeout(function() { $(".alert").remove(); }, 5000);
    }else{

        $('body').addClass("blur");
        $('body').prepend('<div id="loader"></div>');

        $.ajax({
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + authToken);
            },
            url: APIBASEURL+ '/interest/image-link',
            method: 'PUT',
            data: "interest-id="+category+"&image-link="+imageUrl
        })
            .done(function (response) {
                var responseObj = JSON.parse(response);
                if (responseObj.status) {
                    $("#loader").remove();
                    $('body').removeClass("blur");
                    $('.main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>Failed to update category image, please try again later!</div>');
                    setTimeout(function() { $(".alert").remove(); }, 5000);

                } else {
                     fetchCategory();
                    $("#loader").remove();
                    $('body').removeClass("blur");
                    $('.main').prepend('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Info: </strong>Image-Url updated successfully.</div>');
                    setTimeout(function() { $(".alert").remove(); }, 5000);
                }
            })
            .fail(function (response) {
                $("#loader").remove();
                $('body').removeClass("blur");
                $('.main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>Failed to Update Category Image, please try again later!</div>');
                setTimeout(function() { $(".alert").remove(); }, 5000);

            });


}

}

/**
* this function will be invoked when the create button
* on the add category modal is been pressed
* */
function addCategory() {
    var newCatName = $('#newCatName').val();
    var newCatImage = $('#newCatImage').val();
    if(newCatName === "" || newCatImage === "" ){
        $('#modalMessage').append('Please enter category name and category image url.');
        $('#modalMessage').addClass('bg-info');
        setTimeout(function () {
            $('#modalMessage').addClass('hide');
        }, 3000);
    } else {
        $('body').addClass("blur");
        $('body').prepend('<div id="loader"></div>');
        $.ajax({
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + authToken);
            },
            url: APIBASEURL+ '/interest?',
            method: 'POST',
            data:  "interest-name="+newCatName+"&photo-url="+newCatImage
        })
        .done(function (response) {
            var responseObj = JSON.parse(response);
            if (responseObj.status) {
                $("#loader").remove();
                $('body').removeClass("blur");
                $('#modalMessage').append('Failed to add category, please try again later');
                $('#modalMessage').addClass('bg-danger');
                setTimeout(function () {
                    $('#modalMessage').addClass('hide');
                }, 3000);
            } else {
                $("#loader").remove();
                $('body').removeClass("blur");
                $('#modalMessage').append('Category has been added successfully.');
                $('#modalMessage').addClass('text-success');
                if(Array.isArray(categoriesArray)) {
                    categoriesArray.push(responseObj);
                    LoadData();
                }
                setTimeout(function () {
                    $('#message').addClass('hide');
                }, 3000);
                document.getElementById('newCatName').value = "";
                document.getElementById('newCatImage').value = "";

            }
        })
        .fail(function (response) {
            $("#loader").remove();
            $('body').removeClass("blur");
            $('#modalMessage').append('Failed to add category, please try again later');
            $('#modalMessage').addClass('bg-danger');
            setTimeout(function () {
                $('#modalMessage').addClass('hide');
            }, 3000);
        });
    }
}

/**
* this function will be triggered by the click of add subcategory button
*/
function addSubcategory() {
    var subcat = $('#intSubcategory').val();
    var category = $('#intCategory').val();

    if(category == "")

    return alert('Please select the category for this subcategory');

    if (subcat === ""){
        $('.main').prepend('<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Info!</strong>Please enter subcategory name </div>');
        setTimeout(function() { $(".alert").remove(); }, 5000);
    } else {
        $('body').addClass("blur");
        $('body').prepend('<div id="loader"></div>');
        $.ajax({
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + authToken);
            },
            url: APIBASEURL+ '/sub-interest',
            method: 'POST',
            data: "interest-id="+category+"&interest-name="+subcat
        })
        .done(function (response) {
            var responseObj = JSON.parse(response);
            if (responseObj.status) {
                $("#loader").remove();
                $('body').removeClass("blur");
                $('#message').append('Failed to add subcategory, please try again later');
                $('#message').addClass('bg-danger');
                setTimeout(function () {
                    $('#message').addClass('hide');
                }, 3000);

            } else {
                $("#loader").remove();
                $('body').removeClass("blur");
                $('#message').append('SubCategory has been added successfully.');
                $('#message').addClass('bg-success');
                fetchSubInterest();
                setTimeout(function () {
                    $('#message').addClass('hide');
                }, 5000);
                document.getElementById('intSubcategory').value = "";
            }
        })
        .fail(function (response) {
            $("#loader").remove();
            $('body').removeClass("blur");
            $('#message').append('Failed to add subcategory, please try again later');
            $('#message').addClass('bg-danger');
            setTimeout(function () {
                $('#message').addClass('hide');
            }, 3000);
        });
    }
}
