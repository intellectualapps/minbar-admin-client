/*
* User Account management
*/

if(document.cookie.indexOf("pentor_authToken")===-1){
    window.location = "/";
}

$(document).ready(function() {
    setActiveUserEmail();

    $('#searchInput').on("keypress", function(event) {
        if(event.which === 13) {
            $('#search').trigger("click");
        }
    });

    $('#search').on("click", function(event) {
        event.preventDefault();
        $('body').addClass("blur");
        $('body').prepend('<div id="loader"></div>');
        var searchInput = $('#searchInput').val();
        if(searchInput==='' || searchInput===null) {
            $('#pageContent').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Please enter first name, surname or phone number to search for user profiles</strong></div>');
            $("#loader").remove();
            $('body').removeClass("blur");
            setTimeout(function() { $(".alert").remove(); }, 10000);
            return false;
        }
        searchUser(searchInput);
    });

    $("#result_list, #connections_result_list").on("click", ".clickable", function(event) {
        var location = $(this).data("url");
        if(location==="view-connection") {
            var connectionId = jQuery(this).attr("id");
            setCookie("pentor_connection_id", connectionId, 1);
            window.location = location;
        } else {
            var clickedUserEmail = jQuery(this).attr("id");
            setCookie("pentor_clicked_email", clickedUserEmail, 1);
            window.location = location;
        }
    });

    $('#close').on("click", function(event) {
        event.preventDefault();
        deleteCookie("pentor_clicked_email");
        window.location = "/user-profile";
    });

    $('#saveUserDetails').on("click", function(event) {
        event.preventDefault();
        $('body').addClass("blur");
        $('body').prepend('<div id="loader"></div>');
        var email = getCookie('pentor_clicked_email');
        var firstName = $('#firstname').val();
        var lastName = $('#surname').val();
        var birthday = $('#birthday').val().split("-").reverse().join("-");
        var phoneNumber = $('#phone_number').val();
        var isMentor = $('#isMentor').val();
        switch ($('#gender').val()) {
            case 'FEMALE':
            var gender = 'f';
            break;
            case 'MALE':
            var gender = 'm';
            break;
            default:
        }
        var shortBio = $('#short_bio').val();
        var longBio = $('#long_bio').val();
        var address = $('#address').val();
        if(email==='' || firstName==='' || lastName==='' || birthday==='' || phoneNumber==='') {
            $('#pageContent').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Surname, first name, birthday, phoneNumber cannot be empty!</strong></div>');
            $("#loader").remove();
            $('body').removeClass("blur");
            setTimeout(function() { $(".alert").remove(); }, 5000);
            return false;
        }
        updateUserDetails(email, firstName, lastName, birthday, phoneNumber, isMentor, gender, shortBio, longBio, address);
    });

    $('#getConnections').on("click", function(event) {
        event.preventDefault();
        window.location = "/user-connections";
    });
    
    $('#deleteUser').on("click", function(event) {
        event.preventDefault();
        var email = getCookie("pentor_clicked_email");
        var confirmation = confirm("Are you sure you want to delete user with email:\n"+email+"?");
        if (confirmation === true) {
            deleteUser(email);
        } else {
            return false;
        }
    });

});

var authToken = getCookie('pentor_authToken');

function setActiveUserEmail() {
    var activeUserEmail = getCookie('pentor_name');
    $('#activeUser').append(activeUserEmail);
}

function searchUser(searchInput) {
    var searchResult = [];
    $.ajax({
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer "+authToken);
        },
        url: 'https://minbarapp-admin.appspot.com/api/v1/user/search/'+searchInput,
        method: 'GET'
    })
    .done(function(response) {
        var responseObj = JSON.parse(response);
        if(responseObj.status) {
            $('body').removeClass("blur");
            $("#loader").remove();
        } else {
            $('#textContent').attr('hidden', true);
            updateSearchResultTable(responseObj);
        }
    })
    .fail(function(response) {
        var responseObj = JSON.parse(response);
        $('.main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong> '+responseObj.message+'</div>');
        setTimeout(function() { $(".alert").remove(); }, 5000);
        $('body').removeClass("blur");
        $("#loader").remove();
    });
}

function updateSearchResultTable(searchResult) {
    $('#result_list').html('');
    var count = 1;
    var table = document.getElementById("result_list");

    var tHead = document.createElement("thead");
    var headerTr = document.createElement("tr");
    var serialNoHeaderTh = document.createElement("th");
    serialNoHeaderTh.innerHTML = "S/No.";
    var firstNameHeaderTh = document.createElement("th");
    firstNameHeaderTh.innerHTML = "First Name";
    var lastNameHeaderTh = document.createElement("th");
    lastNameHeaderTh.innerHTML = "Last Name";
    var emailHeaderTh = document.createElement("th");
    emailHeaderTh.innerHTML = "Email Address";
    var phoneNumberHeaderTh = document.createElement("th");
    phoneNumberHeaderTh.innerHTML = "Phone Number";
    var genderHeaderTh = document.createElement("th");
    genderHeaderTh.innerHTML = "Gender";

    headerTr.appendChild(serialNoHeaderTh);
    headerTr.appendChild(firstNameHeaderTh);
    headerTr.appendChild(lastNameHeaderTh);
    headerTr.appendChild(emailHeaderTh);
    headerTr.appendChild(phoneNumberHeaderTh);
    headerTr.appendChild(genderHeaderTh);
    tHead.appendChild(headerTr);
    table.appendChild(tHead);

    var tbody = document.createElement("tbody");
    tbody.id = "result-list-tbody";

    var searchResultLength = searchResult.searchResults.length;
    if (searchResultLength > 0) {
        for(var i = 0; i < searchResultLength; i++){
            var firstName = searchResult.searchResults[i].firstName;
            var lastName = searchResult.searchResults[i].lastName;
            var email = searchResult.searchResults[i].email;
            var phoneNumber = searchResult.searchResults[i].phoneNumber;
            var gender = searchResult.searchResults[i].gender;

            if (firstName === "" || firstName === null) {
                firstName = "na";
            }
            if (lastName === "" || lastName === null) {
                lastName = "na";
            }
            if (phoneNumber === "" || phoneNumber === null) {
                phoneNumber = "na";
            }
            if (gender === "" || gender === null) {
                gender = "na";
            }

            var tr = document.createElement("tr");
            tr.className = "clickable";
            tr.id = email;
            tr.setAttribute("data-url", "view-user");

            var countTd = document.createElement("td");
            countTd.innerHTML = count++;

            var firstNameTd = document.createElement("td");
            firstNameTd.innerHTML = firstName;

            var lastNameTd = document.createElement("td");
            lastNameTd.innerHTML = lastName;

            var emailTd = document.createElement("td");
            emailTd.innerHTML = email;

            var phoneNumberTd = document.createElement("td");
            phoneNumberTd.innerHTML = phoneNumber;

            var genderTd = document.createElement("td");
            genderTd.innerHTML = gender;

            tr.appendChild(countTd);
            tr.appendChild(firstNameTd);
            tr.appendChild(lastNameTd);
            tr.appendChild(emailTd);
            tr.appendChild(phoneNumberTd);
            tr.appendChild(genderTd);

            tbody.appendChild(tr);
        }
        var tb_st = true;
    } else {
        var tr = document.createElement("tr");
        var td = document.createElement("td");
        td.innerHTML = "<h1>No users found</h1>";
        td.className = "center";
        td.setAttribute("colspan", "7");
        tr.appendChild(td);
        tbody.appendChild(tr);
        var tb_st = false;
    }
    table.appendChild(tbody);

    if(tb_st) {
        $('#result_list').DataTable({
            destroy: true,
            searching: false,
            ordering:  false
        });
    }

    $('body').removeClass("blur");
    $("#loader").remove();
}

function getUserDetails(email) {
    $('body').addClass("blur");
    $('body').prepend('<div id="loader"></div>');
    $.ajax({
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + authToken);
        },
        url: 'https://minbarapp-admin.appspot.com/api/v1/user',
        method: 'GET',
        data: {'email':email}
    })
    .done(function(response) {
        var responseObj = JSON.parse(response);
        if(responseObj.status) {
            window.location = './user-profile';
        } else {
            populateUserDetails(responseObj);
        }
    })
    .fail(function(response) {
        var responseObj = JSON.parse(response);
        $('.main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong> '+responseObj.message+'</div>');
        setTimeout(function() { $(".alert").remove(); }, 5000);
        $('body').removeClass("blur");
        $("#loader").remove();
    });
}

function populateUserDetails(user_details) {
    var profilePhotoUrl = user_details.profilePhotoUrl;
    var src = "";
    if(profilePhotoUrl && profilePhotoUrl.includes("pentor-bff")) {
        src = "data:image\/(png|jpg);base64,";
        $.ajax({
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + authToken);
            },
            url: profilePhotoUrl,
            method: 'GET'
        })
        .done(function(response) {
            var responseObj = JSON.parse(response);
            src += responseObj.base64ImageString;
            $('#profile_photo').attr("src", src);
        })
        .fail(function(response) {

        });
    } else {
        src = profilePhotoUrl;
        $('#profile_photo').attr("src", src);
    }
    var lastName = user_details.lastName;
    var firstName = user_details.firstName;
    var birthday = user_details.birthday;
    if(birthday) {
        var dob = birthday.substring(0, 10);
    } else {
        var dob = "1900-01-01";
    }
    var gender = user_details.gender;
    var phoneNumber = user_details.phoneNumber;
    var shortBio = user_details.shortBio;
    var longBio = user_details.longBio;
    var address = user_details.address;
    var isMentor = user_details.isMentor;

    $('#user_name').text(firstName + " " + lastName);
    $('#surname').val(lastName);
    $('#firstname').val(firstName);
    $('#birthday').val(dob);

    if(user_details.gender)
        document.getElementById(gender).selected = true;

    $('#phone_number').val(phoneNumber);
    $('#short_bio').val(shortBio);
    $('#long_bio').val(longBio);
    $('#address').val(address);
    document.getElementById(isMentor).selected = true;

    $("#loader").remove();
    $('body').removeClass("blur");
}

function updateUserDetails(email, firstName, lastName, birthday, phoneNumber, isMentor, gender, shortBio, longBio, address) {
    $.ajax({
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + authToken);
        },
        url: "https://minbarapp-admin.appspot.com/api/v1/user/basic",
        method: 'PUT',
        data: {"email":email, "first-name":firstName, "last-name":lastName, "birthday":birthday, "phone-number":phoneNumber, "is-mentor":isMentor, "gender":gender, "short-bio":shortBio, "long-bio":longBio, "address":address}
    })
    .done(function(response) {
        responseObj = JSON.parse(response);
        if (responseObj.status) {
            $('#pageContent').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>'+responseObj.message+'</strong></div>');
            $("#loader").remove();
            $('body').removeClass("blur");
            setTimeout(function() { $(".alert").remove(); }, 10000);
        } else {
            $('#pageContent').prepend('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>User Account update successful</strong></div>');
            $("#loader").remove();
            $('body').removeClass("blur");
            setTimeout(function() { location.reload(); }, 1000);
        }
    })
    .fail(function(response) {
        var responseObj = JSON.parse(response);
        $('.main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong> '+responseObj.message+'</div>');
        setTimeout(function() { $(".alert").remove(); }, 5000);
        $("#loader").remove();
        $('body').removeClass("blur");
    });
}

function getUserConnections(email) {
    $('body').addClass("blur");
    $('body').prepend('<div id="loader"></div>');
    $.ajax({
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + authToken);
        },
        url: 'https://minbarapp-admin.appspot.com/api/v1/mentor/connection/'+email,
        method: 'GET'
    })
    .done(function(response) {
        var responseObj = JSON.parse(response);
        if(responseObj.status) {
            window.location = "./view-user";
        } else {
            updateUserConnectionsTable(responseObj);
        }
    })
    .fail(function(response) {
        var responseObj = JSON.parse(response);
        $('.main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong> '+responseObj.message+'</div>');
        setTimeout(function() { $(".alert").remove(); }, 5000);
        $('body').removeClass("blur");
        $("#loader").remove();
    });
}

function updateUserConnectionsTable(connectionsList) {
    $('#connections_result_list').html('');
    var count = 1;
    var table = document.getElementById("connections_result_list");

    var tHead = document.createElement("thead");
    var headerTr = document.createElement("tr");
    var serialNoHeaderTh = document.createElement("th");
    serialNoHeaderTh.innerHTML = "S/No.";
    var mentorHeaderTh = document.createElement("th");
    mentorHeaderTh.innerHTML = "Mentor";
    var menteeHeaderTh = document.createElement("th");
    menteeHeaderTh.innerHTML = "Mentee";
    var creationHeaderTh = document.createElement("th");
    creationHeaderTh.innerHTML = "Creation Date";
    var statusHeaderTh = document.createElement("th");
    statusHeaderTh.innerHTML = "Status";
    var actionHeaderTh = document.createElement("th");
    actionHeaderTh.innerHTML = "Action";

    headerTr.appendChild(serialNoHeaderTh);
    headerTr.appendChild(mentorHeaderTh);
    headerTr.appendChild(menteeHeaderTh);
    headerTr.appendChild(creationHeaderTh);
    headerTr.appendChild(statusHeaderTh);
    headerTr.appendChild(actionHeaderTh);
    tHead.appendChild(headerTr);
    table.appendChild(tHead);

    var tbody = document.createElement("tbody");
    tbody.id = "result-list-tbody";

    var connectionsListLength = connectionsList.connections.length;
    if (connectionsListLength > 0) {
        for(var i = 0; i < connectionsListLength; i++) {
            var connectionId = connectionsList.connections[i].connectionId;
            var mentor = connectionsList.connections[i].mentor.firstName + " " +connectionsList.connections[i].mentor.lastName;
            var mentorEmail = connectionsList.connections[i].mentor.email;
            var mentee = connectionsList.connections[i].mentee.firstName + " " +connectionsList.connections[i].mentee.lastName;
            var menteeEmail = connectionsList.connections[i].mentee.email;
            var creationDate = connectionsList.connections[i].creationDate;
            var status = connectionsList.connections[i].currentStatus;

            if(mentor === "" || mentor === null) {
                mentor = "na";
            }
            if(mentee === "" || mentee === null) {
                mentee = "na";
            }
            if(creationDate === "" || creationDate === null) {
                creationDate = "na";
            }
            if(status === "" || status === null) {
                status = "na";
            }

            var tr = document.createElement("tr");

            var countTd = document.createElement("td");
            countTd.innerHTML = count++;

            var mentorTd = document.createElement("td");
            mentorTd.className = "clickable";
            mentorTd.id = mentorEmail;
            mentorTd.setAttribute("data-url", "view-user");
            mentorTd.innerHTML = mentor;

            var menteeTd = document.createElement("td");
            menteeTd.className = "clickable";
            menteeTd.id = menteeEmail;
            menteeTd.setAttribute("data-url", "view-user");
            menteeTd.innerHTML = mentee;

            var creationDateTd = document.createElement("td");
            creationDateTd.innerHTML = creationDate;

            var statusTd = document.createElement("td");
            statusTd.innerHTML = status;

            var actionTd = document.createElement("td");
            var usersConnectionId = JSON.stringify(connectionsList.connections[i].connectionId);
            actionTd.innerHTML = "<button type='button' onclick='viewConnection("+usersConnectionId+")' class='btn btn-link'>View</button>";

            tr.appendChild(countTd);
            tr.appendChild(mentorTd);
            tr.appendChild(menteeTd);
            tr.appendChild(creationDateTd);
            tr.appendChild(statusTd);
            tr.appendChild(actionTd);

            tbody.appendChild(tr);
        }
        var tb_st = true;
    } else {
        var tr = document.createElement("tr");
        var td = document.createElement("td");
        td.innerHTML = "<h1>No connections found</h1>";
        td.className = "center";
        td.setAttribute("colspan", "7");
        tr.appendChild(td);
        tbody.appendChild(tr);
        var tb_st = false;
    }
    table.appendChild(tbody);

    if(tb_st) {
        $('#connections_result_list').DataTable({
            destroy: true,
            searching: false,
            ordering:  false
        });
    }

    $('body').removeClass("blur");
    $("#loader").remove();
}

/*
 * this function is triggered by the click of the view button on the connection table
 * */
function viewConnection(usersConnectionId) {
    //using btoa to convert the stingified object into base64 and stored in the local storage
    localStorage.setItem('data', usersConnectionId);

    //routing to the next view-connection page
    window.location = "/view-connection";
}

/*
 * this function is triggered by the click of the delete button on the view user's profile
 */
function deleteUser(email) {
    $('body').addClass("blur");
    $('body').prepend('<div id="loader"></div>');
    $.ajax({
        url: 'https://minbarapp-admin.appspot.com/api/v1/user?email='+email,
        method: 'DELETE'
    })
    .done(function(response) {
        console.log(response);
        if(response) {
            $('.main').html('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success!</strong> User Deleted</div>'); 
            setTimeout(function() { window.location = "/user-profile"; }, 2000);
        } else {
            var responseObj = JSON.parse(response);
            $('.main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error!</strong> '+responseObj.message+'</div>');
            setTimeout(function() { $(".alert").remove(); }, 5000);
        }
        $('body').removeClass("blur");
        $("#loader").remove();
    })
    .fail(function(response) {
        $('.main').prepend('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong> '+response.message+'</div>');
        setTimeout(function() { $(".alert").remove(); }, 5000);
        $('body').removeClass("blur");
        $("#loader").remove();
    });
}
