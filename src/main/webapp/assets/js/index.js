$(document).ready(function() {
    $('#loginForm').on("submit", function(event) {
        event.preventDefault();
        var email = $('#email').val();
        var password = $('#password').val();
        $('body').prepend('<div id="loader"></div>');
        $('#myDiv').attr("hidden", true);
        loginUser(email, password);
    });

    $('#reset-password-button').on("click", function(event) {
        var message = $('#message-wrapper');
        var email = $('#encoded-email').val();
        var token = $('#password-reset-token').val();
        var password = $('#user-new-password').val();
        var password_confirm = $('#user-new-password-confirm').val();
        if(password !== password_confirm)
            return message.html('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong> &nbsp Passwords Not Matched</div>');

        //add bluring and loading
        $('body').addClass("blur");
        $('body').prepend('<div id="loader"></div>');

        var dataString = 'email='+email+'&token='+token+'&password='+password;
        $.ajax({
            method : "PUT",
            url : "https://minbarapp-admin.appspot.com/api/v1/user/password",
            data : dataString
        })
            .done(function (result) {
                var response = JSON.parse(result);
                if (response.status) {
                    $('#message-wrapper').html('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Oops!</strong><p>'+response.message+'</p></div>');
                } else {
                    $('#message-wrapper').html('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Successful</strong><p>Your password reset was successful.</p></div>');
                    setTimeout(function() { $('#message-wrapper').html('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Please wait!</strong><p>Redirecting....</p></div>'); }, 2000);
                    setTimeout(function() { window.location = '/password-changed.html'; }, 2000);

                }
                //remove blur and loading
                $("#loader").remove();
                $('body').removeClass("blur");
            })
            .fail(function (result) {
                //remove blur and loading
                $("#loader").remove();
                $('body').removeClass("blur");
                $('#message-wrapper').html('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Oops!</strong><p>Something went wrong. Please try again!</p></div>');

            });
        event.preventDefault();
    });
});

function loginUser(email, password) {
    $.ajax({
        url: 'https://minbarapp-admin.appspot.com/api/v1/user/authenticate',
        method: 'POST',
        data: "email="+email+"&password="+password
    })
    .done(function(response) {
        var responseObj = JSON.parse(response);
        if(responseObj.status) {
            $('#loginForm').html('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Oops!</strong> '+responseObj.message+'</div>');
            $('#myDiv').attr("hidden", false);
            $("#loader").remove();
        } else {
            setCookie("pentor_email", responseObj.email, 1);
            setCookie("pentor_name", responseObj.firstName+' '+responseObj.lastName, 1);
            setCookie("pentor_authToken", responseObj.authToken, 1);
            setTimeout(function() { window.location = '/user-profile' }, 2000);
        }
    })
    .fail(function(response) {
        var responseObj = JSON.parse(response);
        $('#loginForm').html('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong> '+responseObj.message+'</div>');
        $('#myDiv').attr("hidden", false);
        $("#loader").remove();
    });
    setTimeout(function() { $(".alert").remove(); }, 10000);
}    
