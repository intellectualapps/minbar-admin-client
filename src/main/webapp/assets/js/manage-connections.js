/*Setting constant variables that be used around this script*/
const APIBASEURL = 'https://minbarapp-admin.appspot.com/api/v1';
var authToken = getCookie('pentor_authToken');
var months=['Jan','Feb','Mar','April','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
/************************************************************/

/*Check if the cookie in the user browser contains pentor_authToken else throw him out*/
if (document.cookie.indexOf("pentor_authToken") === -1) {
    window.location = "/";
}
/*************************************************************************************/

/*contents of this function will only load when the DOM is ready for manipulation*/
$(document).ready(function () {
    setActiveUserEmail();

    /**
    * An IIFE for fetching category on page load
    *
    * */
    (function () {
        $.ajax({
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + authToken);
            },
            url: APIBASEURL +'/interest',
            method: 'GET'
        })
        .done(function (response) {
            var responseObj = JSON.parse(response);
            if (responseObj.status) {
                /* $('body').removeClass("blur");
                $("#loader").remove();*/
            } else {
                var $select = $('#category');
                $.each(responseObj.interests, function (key, value) {
                    $select.append('<option value=' + value.id + '>' + value.name + '</option>');
                });

            }
        })
        .fail(function (response) {
            var responseObj = JSON.parse(response);
            $('.main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>'+responseObj.message+'</div>');
            setTimeout(function() { $(".alert").remove(); }, 5000);
        });
    })();
});

/*
* adds the user email to the dashboard
* */
function setActiveUserEmail() {
    var activeUserEmail = getCookie('pentor_name');
    $('#activeUser').append(activeUserEmail);


}


/*
* this function fetches subinterest based on the category selected by the user.
* */
function fetchSubInterest() {
    var loader = $('#loadSubInterest');
    loader.removeClass('hide');
    var category = $("#category").val();

    $.ajax({
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + authToken);
        },
        url: APIBASEURL + '/interest/sub?id=' + category,
        method: 'GET'
    })
    .done(function (response) {
        var responseObj = JSON.parse(response);
        if (responseObj.status) {
            $('.main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>'+responseObj.message+'</div>');
            setTimeout(function() { $(".alert").remove(); }, 5000);
            loader.addClass('hide');
        } else {
            var $subCategory = $('#subCategory');
            $.each(responseObj.subInterests, function (key, value) {
                $subCategory.append('<option value=' + value.subId + '>' + value.name + '</option>');
            });
            loader.addClass('hide');
        }
    })
    .fail(function (response) {
        loader.addClass('hide');
        $('.main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>Failed to load sub-category. Please Refresh your browser!</div>');
        setTimeout(function() { $(".alert").remove(); }, 5000);
    });
}

/*
* this function is triggered by the click of search connection button
* takes the subinterest id to fetch the connections
* */
function searchConnections() {
    $('body').addClass("blur");
    $('body').prepend('<div id="loader"></div>');
    var param = $("#subCategory").val();
    var connectionList = [];

    $.ajax({
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + authToken);
        },
        url: APIBASEURL+ '/mentor/link/sub-interest/'+ param,
        method: 'GET'
    })
    .done(function (response) {
        var responseObj = JSON.parse(response);
        if (responseObj.status) {
            $('body').removeClass("blur");
            $("#loader").remove();

        } else {
            $('#textContent').attr('hidden', true);
            updateConnectionsTable(responseObj.connections);

        }
    })
    .fail(function (response) {
        var responseObj = JSON.parse(response);
        $('.main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>'+responseObj.message+'</div>');
        setTimeout(function() { $(".alert").remove(); }, 5000);
        $('body').removeClass("blur");
        $("#loader").remove();
    });
}

/*
* This function populates the connection table with response gotten from search connection function
* */
function updateConnectionsTable(connectionList) {
    $('#result_list').html('');
    var count = 1;
    var table = document.getElementById("result_list");

    var tHead = document.createElement("thead");
    var headerTr = document.createElement("tr");
    var serialNoHeaderTh = document.createElement("th");
    serialNoHeaderTh.innerHTML = "S/No.";
    var mentorHeaderTh = document.createElement("th");
    mentorHeaderTh.innerHTML = "Mentor";
    var menteeHeaderTh = document.createElement("th");
    menteeHeaderTh.innerHTML = "Mentee";
    var creationDateHeaderTh = document.createElement("th");
    creationDateHeaderTh.innerHTML = "Creation Date";
    var statusHeaderTh = document.createElement("th");
    statusHeaderTh.innerHTML = "Status";
    var actionHeaderTh = document.createElement("th");
    actionHeaderTh.innerHTML = "Action";

    headerTr.appendChild(serialNoHeaderTh);
    headerTr.appendChild(mentorHeaderTh);
    headerTr.appendChild(menteeHeaderTh);
    headerTr.appendChild(creationDateHeaderTh);
    headerTr.appendChild(statusHeaderTh);
    headerTr.appendChild(actionHeaderTh);
    tHead.appendChild(headerTr);
    table.appendChild(tHead);

    var tbody = document.createElement("tbody");
    tbody.id = "result-list-tbody";
    var connectionsLength= connectionList.length;
    if (connectionsLength > 0) {

        for(var i = 0; i < connectionsLength; i++){
            var connectionId = connectionList[i].connectionId;
            var mentor = connectionList[i].mentor.firstName + ' '+connectionList[i].mentor.lastName;
            var mentee = connectionList[i].mentee.firstName + ' '+connectionList[i].mentee.lastName;
            var creationDate = connectionList[i].creationDate;
            var status = connectionList[i].currentStatus;

            if (mentor === "" || mentor === null) {
                mentor = "na";
            }
            if (mentee === "" || mentee === null) {
                mentee = "na";
            }
            if (status === "" || status === null) {
                status = "na";
            }
            var myDate = new Date(creationDate);
            var month =months[myDate.getMonth()];
            var year= myDate.getFullYear();
            var day=myDate.getDate();
            if(day<10) day='0'+day;
            var formattedDate=month+' '+day+', '+year;
            var tr = document.createElement("tr");
            tr.id = connectionId;

            var countTd = document.createElement("td");
            countTd.innerHTML = count++;

            var mentorTd = document.createElement("td");
            mentorTd.innerHTML = "<span class='text-primary'>"+mentor+"</span>";

            var menteeTd = document.createElement("td");
            menteeTd.innerHTML = "<span class='text-primary'>"+mentee+"</span>";

            var creationTd = document.createElement("td");
            creationTd.innerHTML = formattedDate;

            var statusTd = document.createElement("td");
            statusTd.innerHTML = status;

            var viewTd = document.createElement("td");
            var usersConnectionId = JSON.stringify(connectionList[i].connectionId);
            viewTd.innerHTML = "<button type='button' onclick='viewConnection("+usersConnectionId+")' class='btn btn-link'>View</button>";

            tr.appendChild(countTd);
            tr.appendChild(mentorTd);
            tr.appendChild(menteeTd);
            tr.appendChild(creationTd);
            tr.appendChild(statusTd);
            tr.appendChild(viewTd);

            tbody.appendChild(tr);

        }
        var tb_st = true;
    } else {
        var tr = document.createElement("tr");
        var td = document.createElement("td");
        td.innerHTML = "<h1>No connection found</h1>";
        td.className = "center";
        td.setAttribute("colspan", "6");
        td.setAttribute("align", "center");
        tr.appendChild(td);
        tbody.appendChild(tr);
        var tb_st = false;
    }
    table.appendChild(tbody);

    if(tb_st) {
        $('#result_list').DataTable({
            destroy: true,
            searching: false,
            ordering:  false
        });
    }

    $('body').removeClass("blur");
    $("#loader").remove();
}

/*
* this function is triggered by the click of the view button on the connection table
* */
function viewConnection(usersConnectionId) {
    //using btoa to convert the stingified object into base64 and stored in the local storage
    localStorage.setItem('data', usersConnectionId);

    //routing to the next view-connection page
    window.location = "/view-connection";
}

/*
* This function populates the view connection page with connection profile.
* */
function usersDetails() {
    $('body').addClass("blur");
    $('body').prepend('<div id="loader"></div>');

    //fetching back the object stored in the local storage and the converting it back to object.
    var usersConnectionId  = localStorage.getItem('data');

    $.ajax({
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + authToken);
        },
        url: APIBASEURL + '/connection/full/' + usersConnectionId,
        method: 'GET'
    })
    .done(function (response) {
        var responseObj = JSON.parse(response);
        if (responseObj.status) {
            $('.main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>Failed to load user-profile. Please refresh your browser!</div>');
            setTimeout(function() { $(".alert").remove(); }, 5000);
        } else {
            var dataObj = responseObj;
            populateInfo(dataObj)
        }
        $('body').removeClass("blur");
        $("#loader").remove();
    })
    .fail(function (response) {
        $('body').removeClass("blur");
        $("#loader").remove();
        var responseObj = JSON.parse(response);
        $('.main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>Failed to load user-profile. Please Refresh your browser!</div>');
        setTimeout(function() { $(".alert").remove(); }, 5000);
    });


}

function populateInfo(dataObj) {
    var created = dataObj.creationDate,
    subcat = dataObj.subInterest.name,
    status = dataObj.currentStatus,
    mentorPix = dataObj.mentor.profilePhotoUrl,
    mentorSurname = dataObj.mentor.lastName,
    mentorFirstname= dataObj.mentor.firstName,
    mentorBirthday = dataObj.mentor.birthday,
    mentorGender = dataObj.mentor.gender,
    mentorPhone = dataObj.mentor.phoneNumber,
    mentorSBio = dataObj.mentor.shortBio,
    mentorLBio = dataObj.mentor.longBio,
    mentorAddress = dataObj.mentor.address,
    mentorStatus = dataObj.mentor.isMentor ? 'Yes' : 'No',
    menteePix = dataObj.mentee.profilePhotoUrl,
    menteeSurname = dataObj.mentee.lastName,
    menteeFirstname= dataObj.mentee.firstName,
    menteeBirthday = dataObj.mentee.birthday,
    menteeGender = dataObj.mentee.gender,
    menteePhone = dataObj.mentee.phoneNumber,
    menteeSBio = dataObj.mentee.shortBio,
    menteeLBio = dataObj.mentee.longBio,
    menteeAddress = dataObj.mentee.address,
    menteeStatus = dataObj.mentee.isMentor ? 'Yes': 'No',
    connectionId =  dataObj.connectionId;


    myDate = new Date(created);
    var month =months[myDate.getMonth()],
    year= myDate.getFullYear(),
    day=myDate.getDate();
    if(day<10) day='0'+day;
    var createdDate=month+' '+day+', '+year;

    /******This will remove the suspend connection button when the connection status is admin_terminated***********************/
    if(status == "ADMIN_TERMINATED")
    $('#suspendBTN').remove();
    /************************************************************************************************************************/
    $('#created').append(createdDate);
    $('#subcat').append(subcat);
    $('#status').append(status);
    $('#id').val(connectionId);

    /****************************Mentor Details************************************/
    var mentorSrc = "";
    if(mentorPix && mentorPix.includes("pentor-bff")) {
        mentorSrc = "data:image\/(png|jpg);base64,";
        $.ajax({
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + authToken);
            },
            url: mentorPix,
            method: 'GET'
        })
        .done(function(response) {
            var responseObj = JSON.parse(response);
            mentorSrc += responseObj.base64ImageString;
            $('#mentorPix').attr("src", mentorSrc);
        })
        .fail(function(response) {
            var responseObj = JSON.parse(response);
            $('.main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>'+responseObj.message+'</div>');
            setTimeout(function() { $(".alert").remove(); }, 5000);

        });
    } else {
        mentorSrc = mentorPix;
        $('#mentorPix').attr("src", mentorSrc);
    }


    if(mentorBirthday) {
        var myDate = new Date(mentorBirthday);
        var month =months[myDate.getMonth()];
        var year= myDate.getFullYear();
        var day=myDate.getDate();
        if(day<10) day='0'+day;
        var mentorDob=month+' '+day+', '+year;

    } else {
        var mentorDob = "Jan-01-1900";
    }

    $('#mentorFullname').text(mentorFirstname + " " + mentorSurname);
    $('#mentorSurname').val(mentorSurname);
    $('#mentorFirstname').val(mentorFirstname);
    $('#mentorBirthday').val(mentorDob);
    $('#mentorGender').val(mentorGender);
    $('#mentorPhone').val(mentorPhone);
    $('#mentorSBio').val(mentorSBio);
    $('#mentorLBio').val(mentorLBio);
    $('#mentorAddress').val(mentorAddress);
    $('#mentorStatus').val(mentorStatus);
    // document.getElementById(mentorStatus).selected = true;

    /******************************************************************************/

    /****************************Mentee Details************************************/
    var menteeSrc = "";
    if(menteePix && menteePix.includes("pentor-bff")) {
        menteeSrc = "data:image\/(png|jpg);base64,";
        $.ajax({
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + authToken);
            },
            url: menteePix,
            method: 'GET'
        })
        .done(function(response) {
            var responseObj = JSON.parse(response);
            menteeSrc += responseObj.base64ImageString;
            $('#menteePix').attr("src", menteeSrc);
        })
        .fail(function(response) {

        });
    } else {
        menteeSrc = menteePix;
        $('#menteePix').attr("src", menteeSrc);
    }

    if(menteeBirthday) {
        var myDate = new Date(menteeBirthday);
        var month =months[myDate.getMonth()];
        var year= myDate.getFullYear();
        var day=myDate.getDate();
        if(day<10) day='0'+day;
        var menteeDob=month+' '+day+', '+year;
    } else {
        var menteeDob = "Jan-01-1900";
    }

    $('#menteeFullname').text(menteeFirstname + " " + menteeSurname);
    $('#menteeSurname').val(menteeSurname);
    $('#menteeFirstname').val(menteeFirstname);
    $('#menteeBirthday').val(menteeDob);
    $('#menteeGender').val(menteeGender);
    $('#menteePhone').val(menteePhone);
    $('#menteeSBio').val(menteeSBio);
    $('#menteeLBio').val(menteeLBio);
    $('#menteeAddress').val(menteeAddress);
    $('#menteeStatus').val(menteeStatus);
    // document.getElementById(menteeStatus).selected = true;

    /******************************************************************************/

    $("#loader").remove();
    $('body').removeClass("blur");

}
/*
* This function will be called when the user clicks the suspend button.
* */
function suspendCon() {
    var connectionId = $('#id').val();
    var reason = $('#reason').val();
    var message  = $('#suspendedMessage');

    if(reason === "")
    return message.prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>Please provide reason for suspension.</div>');
    $('body').addClass("blur");
    $('body').prepend('<div id="loader"></div>');

    $.ajax({
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + authToken);
        },
        url: APIBASEURL+ '/mentor/connection/suspend/'+connectionId,
        method: 'PUT',
        data: "reason="+reason
    })
    .done(function (response) {
        var responseObj = JSON.parse(response);
        if (responseObj.status) {
            $("#loader").remove();
            $('body').removeClass("blur");
            message.prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>Suspension of connection failed, please try again later</div>');

        } else {
            $("#loader").remove();
            $('body').removeClass("blur");
            message.prepend('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Info!</strong>The Connection has been suspended successfully.</div>');
            setTimeout(function(){
                window.location = '/view-connection';
            }, 3000);
        }
    })
    .fail(function (response) {
        $("#loader").remove();
        $('body').removeClass("blur");
        message.prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>Suspension of connection failed, please try again later</div>');
        setTimeout(function() { $(".alert").remove();
        }, 5000);
    });
}
