/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minbar.admin.client;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author buls
 */
public class PasswordReset extends HttpServlet {    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String encodedEmail = request.getParameter("i");
        String token = request.getParameter("t");
       
        request.setAttribute("email", encodedEmail);
        request.setAttribute("token", token);
        
        request.getRequestDispatcher("WEB-INF/pages/reset-password.jsp").forward(request, response);              
    }

   
    @Override
    public String getServletInfo() {
        return "Handles password reset requests";
    }

}
